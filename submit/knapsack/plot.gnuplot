set title "Knapsack";
set xlabel "Time";
set ylabel "Items";
set terminal canvas font ", 8"
set tics font ",8"
set output "../graphs/knapsack.html"
plot "knapsack/graph-data.txt" with lines, x*log(x)/21
set terminal svg
set output "../graphs/knapsack.svg"
replot
