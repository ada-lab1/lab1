set title "MergeSort";
set ylabel "Time";
set xlabel "Elements";
set terminal canvas font ", 8"
set tics font ",8"
set output "../graphs/merge_sort.html"
plot "merge_sort/graph-data.txt" using 1:2 with lines, x*log(x)/6.7
set terminal svg
set output "../graphs/merge_sort.svg"
replot
