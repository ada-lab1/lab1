set title "MCM";
set xlabel "No of Matrices";
set ylabel "Time";
set terminal canvas font ", 8"
set tics font ",8"
set output "../graphs/mcm.html"
plot "mcm/graph-data.txt" with lines, 0.39*x**3
set terminal svg
set output "../graphs/mcm.svg"
replot
