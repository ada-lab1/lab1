#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstring>
#include <deque>
#include <iomanip>
#include <iostream>
#include <limits>
#include <random>
#include <sys/resource.h>
#include <type_traits>
#include <vector>

#define TOOMUCHRECURSION 1

static std::chrono::steady_clock clk;

struct AsmNode {
  double worktime;   // contains time used to complete the task on a particular node
  double transferUp; // contains time used to transfer from this node to the line above it
  double
      transferDown; // contains time used to transfer from this node to the line below it
};
struct AsmLine {
  std::deque<AsmNode> nodes; // list of all nodes
  double entrytime;          // entrytime for the line
  double exittime;           // exit time for the line *unimplemented*
  AsmLine() = default;
  AsmLine(std::deque<AsmNode> const& p_nodes, double const& p_entrytime,
          double const& p_exittime)
      : nodes(p_nodes), entrytime(p_entrytime), exittime(p_entrytime) {}
  AsmLine(std::deque<AsmNode>&& p_nodes, double&& p_entrytime, double&& p_exittime)
      : nodes(p_nodes), entrytime(p_entrytime), exittime(p_exittime) {}
};
template <bool _DP = true> struct Result;
template <> struct Result<true> {
  std::deque<std::deque<double>>
      costs; // matrix that stores the best cost to come to any node *dp only*
  std::deque<std::deque<long>>
      before;     // matrix that stores the best path recursively(in reverse order)
  long chosenone; // end point of the best path
};
template <> struct Result<false> {
  std::deque<std::deque<long>> before;
  long chosenone;
};

template <bool _DP = true>
double costintime(std::deque<AsmLine> const& asmlines, long line, long node,
                  Result<_DP>& res) {
  if (node == 0) {
    // populate table if using dp
    if constexpr (_DP) {
      res.costs[line][node] =
          asmlines[line].entrytime + asmlines[line].nodes[node].worktime;
    } else {
      // return if not using dp
      return asmlines[line].entrytime + asmlines[line].nodes[node].worktime;
    }
  }
  // if using dp return from table
  if constexpr (_DP == true) {
    if (res.costs[line][node] != -1)
      return res.costs[line][node];
  }
  // 3 options continue up, down or straight
  std::array<double, 3> options;
  std::fill(options.begin(), options.end(), std::numeric_limits<double>::max());

  options[0] = costintime<_DP>(asmlines, line, node - 1, res) +
               asmlines[line].nodes[node].worktime;
  if (line > 0) {
    options[2] = costintime<_DP>(asmlines, line - 1, node - 1, res) +
                 asmlines[line - 1].nodes[node - 1].transferDown;
  }
  if (line < (asmlines.size() - 1)) {
    options[1] = costintime<_DP>(asmlines, line + 1, node - 1, res) +
                 asmlines[line + 1].nodes[node - 1].transferUp;
  }
  auto min = std::min_element(options.begin(), options.end());
  if constexpr (_DP) {
    res.costs[line][node] = *min;
  }
  res.before[line][node] = min - options.begin();
  res.before[line][node] = (res.before[line][node] == 2) ? -1 : res.before[line][node];
  return *min;
}

// calculates schedule using by calling costintime
template <bool _DP = true>
std::pair<Result<_DP>, double> schedule(std::deque<AsmLine> const& asmlines) {
  Result<_DP> res;
  // make basic res
  // res.costs all set to -1
  // res.before all set to 3
  for (int i = 0; i < asmlines.size(); ++i) {
    // costs only exists when using dp
    if constexpr (_DP) {
      res.costs.push_back({});
    }
    res.before.push_back({});
    for (int j = 0; j < asmlines[0].nodes.size(); ++j) {
      if constexpr (_DP) {
        res.costs[i].push_back(-1);
      }
      res.before[i].push_back(3);
    }
  }
  // calls costintime for iteratively for endpoints of all the lines
  double min = std::numeric_limits<double>::max(), temp;
  for (int i = 0; i < asmlines.size(); ++i) {
    temp = costintime(asmlines, i, asmlines[i].nodes.size() - 1, res);
    if (temp < min) {
      min           = temp;
      res.chosenone = i;
    }
  }
  return {res, min};
}

// returns generator to generate input from randomly
auto randomGen(int lines, int nodes) {
  return [lines, nodes]() {
    static std::random_device randdev;
    static std::mt19937 rng(randdev());
    static std::uniform_int_distribution<int> dist(1, std::numeric_limits<int>::max());
    // static std::uniform_int_distribution<int> dist(1, 10);
    static auto randNumberGen = std::bind(dist, rng);
    auto randNodeGen          = []() {
      AsmNode node{double(randNumberGen()), double(randNumberGen()),
                   double(randNumberGen())};
      return node;
    };
    AsmLine res(std::deque<AsmNode>(nodes), randNumberGen(), randNumberGen());
    std::generate(res.nodes.begin(), res.nodes.end(), randNodeGen);
    return res;
  };
}

// returns generator to generate input from stdin
auto inputGen(long lines, long nodes) {
  return [lines, nodes]() {
    double temp1, temp2, temp3;
    std::cin >> temp1;
    AsmLine res(std::deque<AsmNode>(nodes), temp1, temp1);
    auto genNode = [nodes, &temp1, &temp2, &temp3]() {
      std::cin >> temp1 >> temp2 >> temp3;
      return AsmNode{temp1, temp2, temp3};
    };
    std::generate(res.nodes.begin(), res.nodes.end(), genNode);
    return res;
  };
}

template <bool _DP = true> void print(Result<_DP> const& res) {
  // reverses recursive chain to iterative chain and gives startingpoint
  auto makeNext = [&res]() -> std::pair<std::deque<std::deque<long>>, long> {
    std::deque<std::deque<long>> next(res.before.size(),
                                      std::deque<long>(res.before[0].size(), 3));
    int befval   = res.before[res.chosenone][res.before[0].size() - 1];
    long curline = res.chosenone;
    int counter  = 0;
    while (befval != 3) {
      ++counter;
      curline += befval;
      next[curline][next[0].size() - 1 - counter] = befval * -1;
      befval = res.before[curline][res.before[0].size() - 1 - counter];
    }
    return {next, curline};
  };

  auto [next, startingpoint] = makeNext();
  std::cout << "Starting Line: " << startingpoint << "\n";
  // a lot of messy things to print stuff using iterative chain
  // TODO: refactor
  int width = int(std::log10(res.before[0].size()) + 1);
  std::string base((width + 1) * res.before[0].size(), ' ');
  for (int i = 0; i < res.before.size(); ++i) {
    auto copy = base;
    for (int j = 0; j < res.before[0].size(); ++j) {
      std::cout << std::setw(width) << j + 1;
      if (next[i][j] == 0) {
        std::cout << "-";
      } else {
        std::cout << " ";
      }
    }
    std::cout << "\n";
    if (i == res.before.size() - 1)
      continue;
    for (int k = i; k <= i + 1; ++k) {
      for (int j = 0; j < res.before[0].size(); ++j) {
        if (next[k][j] == 1 && k == i)
          copy[((width + 1) * j) + width] = '\\';
        else if (next[k][j] == -1 && k > i)
          copy[((width + 1) * j) + width] = '/';
      }
    }
    std::cout << copy << "\n";
  }
}

int main(int argc, char** args) {
#ifdef TOOMUCHRECURSION
  rlimit R;
  getrlimit(RLIMIT_STACK, &R);
  R.rlim_cur = R.rlim_max;
  setrlimit(RLIMIT_STACK, &R);
#endif
  long lines, nodes;
  std::cin >> lines >> nodes;
  std::deque<AsmLine> asmlines(lines);
  std::generate(asmlines.begin(), asmlines.end(), randomGen(lines, nodes));
  auto start          = clk.now();
  auto [res, mintime] = schedule<true>(asmlines);
  auto end            = clk.now();
  std::cerr << lines * nodes << " " << lines << " " << nodes << " "
            << (end - start).count() / 1000 << "\n";
  if (argc == 2) {
    if (std::strcmp(args[1], "-q") == 0) {
      return 0;
    }
  }
  std::cout << "Minimum Time: " << mintime << "\n";
  print(res);
}
