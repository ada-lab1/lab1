#include <algorithm>
#include <chrono>
#include <iostream>
#include <vector>
#include <cstdlib>

std::chrono::high_resolution_clock clk;

int mcm(int i, int j,std::vector<std::vector<int>> &m, 
    std::vector<std::vector<int>> &s, std::vector<int> p) {
  if(i == j ) {
    m[i][j] = 0;
    return m[i][j];
  }
  if(m[i][j] != -1) return m[i][j];
  m[i][j] = INT32_MAX;
  for (int iter1 = i; iter1 < j; ++iter1){
    auto temp = mcm(i, iter1, m, s, p) + mcm(iter1 + 1, j, m, s, p) + p[i-1]*p[iter1]*p[j];
    if(temp < m[i][j]){
      m[i][j] = temp;
      s[i][j] = iter1;
    }
  }
  return m[i][j];

};

void print(std::vector<std::vector<int>> &s, int i , int j){
  if(i == j){
    std::cout << 'A' << i; 
    return;
  }
  std::cout <<  '(';
  print(s, i, s[i][j]);
  print(s, s[i][j] +1, j);
  std::cout <<  ')';
}

int main(int argc, char** args) {
  int psize = 0;
  std::cin >> psize;
  std::vector<int> p;
  p.reserve(psize);
  srand(std::stoll(args[1]));
  std::vector<std::vector<int>> m;
  std::vector<std::vector<int>> s;
  for(int i = 0; i < psize; ++i) {
    p.push_back(rand()%INT32_MAX);
  }
  for(int i = 0 ; i < p.size(); ++i ){
    m.push_back({});
    s.push_back({});
    for(int j = 0; j < p.size(); ++j){
      m[i].push_back(-1);
      s[i].push_back(0);
    }
  }
  auto start = clk.now();
  mcm(1, m.size() - 1 , m, s, p);
  auto end = clk.now();
  std::cerr << p.size() - 1 << " " << (end-start).count()/100 << "\n";
  // for(auto iter : m){
  //   for(auto iter2 : iter){
  //     std::cout << iter2 << " " ;
  //   }
  //   std::cout << "\n";
  // }
  // for(int i = 1; i < m.size(); ++i){
  //   for(int j = 1; j < m.size(); ++j){
  //     std::cout << m[i][j] << " ";
  //   }
  //   std::cout << "\n";
  // }
  // for(int i = 1; i < m.size(); ++i){
  //   for(int j = 1; j < m.size(); ++j){
  //     std::cout << s[i][j] << " ";
  //   }
  //   std::cout << "\n";
  // }
  // std::cout << "\n";
  // print(s, 1, s.size() -1 );
}
