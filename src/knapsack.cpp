#include "include/sort.h"
#include <algorithm>
#include <chrono>
#include <deque>
#include <iostream>

std::chrono::high_resolution_clock clk;

class knapsackElement {
public:
  double m_cost;
  double m_weight;
  double m_costPerkg;
  knapsackElement(double const &cost, double const &weight)
      : m_cost(cost), m_weight(weight), m_costPerkg(cost / weight) {}
  knapsackElement() = default;
};

std::pair<std::deque<knapsackElement>, double> input() {
  int num_elements;
  double total_weight;
  std::cin >> total_weight >> num_elements;
  std::deque<knapsackElement> elements;
  double temp_cost, temp_weight;
  while (num_elements > 0) {
    if (!std::cin) {
      throw std::string("Wrong input file.");
    }
    std::cin >> temp_cost >> temp_weight;
    elements.emplace_back(temp_cost, temp_weight);
    --num_elements;
  }
  return {elements, total_weight};
}

std::deque<knapsackElement> makeKnapsack(std::deque<knapsackElement> &elements,
                                         double total_weight) {
  using namespace std::chrono;
  auto start = clk.now();
  std::deque<knapsackElement> res;
  ds::quick_sort(elements.begin(), elements.end(), 3,
                 [](knapsackElement const &a, knapsackElement const &b) {
                   return a.m_costPerkg > b.m_costPerkg;
                 });
  auto iter = elements.begin();
  while (total_weight > 0 && iter != elements.end()) {
    if (iter->m_weight > total_weight) {
      res.emplace_back(total_weight * iter->m_costPerkg, total_weight);
      break;
    }
    res.push_back(*iter);
    total_weight -= iter->m_weight;
    ++iter;
  }
  auto end = clk.now();
  std::cerr << elements.size() << " " << (end - start).count()/1000  << "\n";
  return res;
}

int main() {
  int cases;
  std::cin >> cases;
  while (cases != 0) {
    auto [elements, total_weight] = input();
    auto knapsack = makeKnapsack(elements, total_weight);
    double totalProfit = 0;
    for (auto iter : knapsack) {
      totalProfit += iter.m_cost;
    }
    std::cout << "Total Profit: " << totalProfit << "\n";
    --cases;
  }
  return 0;
}
