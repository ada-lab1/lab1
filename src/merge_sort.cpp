#include <chrono>
#include <cstdlib>
#include <iostream>
#include <vector>
#include "merge_sort.h"

std::chrono::high_resolution_clock clk;

int main(int argc, char **args) {
  unsigned long size;
  std::cin >> size;
  std::vector<unsigned long> vec1;
  vec1.reserve(size);
  std::srand(std::stoll(args[1]));
  for (unsigned long i = 0; i < size; ++i)
    vec1.push_back(rand() % INT64_MAX);
  auto start = clk.now();
  merge_sort(vec1.begin(), vec1.end());
  auto end = clk.now();
  std::cerr << size << " " << (end - start).count()/1000 << "\n";
}
