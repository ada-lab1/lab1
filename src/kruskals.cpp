#include <algorithm>
#include <cstdint>
#include <iostream>
#include <type_traits>
#include <vector>

template <typename _T> struct edge {
  uint64_t x;
  uint64_t y;
  int64_t weight;
  _T data;
};

template <typename _T> struct input {
  uint64_t vertices;
  std::vector<edge<_T>> edges;
};
template <typename _T> using output = std::vector<edge<_T>>;

template <typename _T> output<_T> kruskals(input<_T> const& in) {
  auto cpyIn = in;
  output<_T> ou;
  auto& edges = cpyIn.edges;
  std::vector<bool> vertices(in.vertices, false);
  std::sort(edges.begin(), edges.end(),
            [](typename std::remove_reference_t<decltype(edges)>::value_type const& a,
               typename std::remove_reference_t<decltype(edges)>::value_type const& b) {
              return a.weight < b.weight;
            });
  for (auto& iter : edges) {
    if (!(vertices[iter.x] ^ vertices[iter.y])) {
      continue;
    }
    vertices[iter.x] = true;
    vertices[iter.y] = true;
    ou.push_back(std::move(iter));
  }
  return ou;
}

template <typename _T> void print(output<_T> const& out) {
  for (auto &iter : out){
    std::cout << iter.x << " " << iter.y << " " << iter.weight << "\n";
  }
}


template <typename _T> auto genIstream(std::istream& is) {
  return [&is]() -> edge<_T> {
    uint64_t temp, temp2;
    int64_t temp3;
    if (is) {
      is >> temp;
      is >> temp2;
      is >> temp3;
    }
    return edge<_T>{temp, temp2, temp3};
  };
}

int main() {
  uint64_t temp1, temp2;
  std::cin >> temp1 >> temp2;
  input<int> in{temp1, std::vector<edge<int>>(temp2)};
  std::generate(in.edges.begin(), in.edges.end(), genIstream<int>(std::cin));
  print(kruskals(in));
}
