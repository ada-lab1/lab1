#ifndef SELECT_SORT
#define SELECT_SORT

#include <iterator>
#include <functional>

template <typename _Iterator,
          typename _Compare = std::less<typename _Iterator::value_type>>
void select_sort(_Iterator first, _Iterator last,
                 _Compare compare = _Compare{})
{
  for (auto iter1 = first; iter1 != last; ++iter1)
  {
    auto anchor = iter1;
    for (auto iter2 = iter1; iter2 != last; ++iter2)
    {
      if (compare(*iter2, *iter1))
        anchor = iter2;
    }
    std::iter_swap(anchor, iter1);
  }
}

#endif
