#include <functional>
#include <iterator>
#include <vector>

template <typename _T, typename _Comp = std::less<typename _T::value_type>>
void merge_sort(_T RandIt1, _T RandIt2, _Comp comp = _Comp{}) {
  static_assert(std::is_same<typename _T::iterator_category,
                             std::random_access_iterator_tag>::value,
                "Iterator must be random access");

  if (RandIt2 - RandIt1 == 1)
    return;

  static auto merge = [&comp](_T start, _T half, _T end) {
    std::vector<typename _T::value_type> vec;
    auto iter1 = start, iter2 = half;
    do {
      if (comp(*iter1, *iter2)) {
        vec.push_back(*iter1);
        ++iter1;
      } else {
        vec.push_back(*iter2);
        ++iter2;
      }
    } while (iter1 != half && iter2 != end);
    vec.insert(vec.end(), iter1, half);
    vec.insert(vec.end(), iter2, end);
    // return vec;
    int i = 0;
    for (auto iter = start; iter != end; ++iter, ++i) {
      *iter = vec[i];
    }
  };

  auto half = RandIt1;

  {
    auto ptrDif = RandIt2 - RandIt1;
    half = RandIt1 + ptrDif / 2;
  }

  merge_sort(RandIt1, half, comp);
  merge_sort(half, RandIt2, comp);
  merge(RandIt1, half, RandIt2);
}
