// Copyright (C) 2021 Himanshu Gupta
//
// This file is part of Data Structures.
//
// Data Structures is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Data Structures is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Data Structures.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SORT_H
#define SORT_H
#include <algorithm>
#include <cstddef>
#include <functional>
#include <iterator>
#include <utility>

namespace ds
{
template <typename _T, typename _Comp = std::less<typename _T::value_type>>
void quick_sort(_T start, _T end, int pivot = 3, _Comp comp = _Comp{})
{
  if (end - start < 2)
    return;
  auto        pivot_iter = end - 1;
  static auto partition  = [&comp](_T start, _T end, _T pivot_iter)
  {
    auto pivot_val = *pivot_iter;
    auto iter1     = start;
    auto iter2     = end - 1;
    for (; iter1 != end && (iter1 < iter2 ); ++iter1)
    {
      if (!comp(*iter1, *pivot_iter))
      {
        for (; iter2 != start - 1 && (iter1 < iter2); --iter2)
        {
          if (comp(*iter2, *pivot_iter))
          {
            std::iter_swap(iter2, iter1);
            break;
          }
        }
      }
    }
    std::iter_swap(pivot_iter, iter2);
    return iter2;
  };
  pivot_iter = partition(start, end, pivot_iter);
  // for(auto it = start; it != end ; ++it)
  // 	std::cout << *it << ' ';
	// 	std::cout << std::endl;
  quick_sort(start, pivot_iter, pivot, comp);
  quick_sort(pivot_iter + 1, end, pivot, comp);
}

} // namespace ds

#endif
