SRCS=$(wildcard src/*.cpp)
TESTS=$(patsubst src/%.cpp,submit/%/graph-data.txt,${SRCS})
PROGS=$(patsubst src/%.cpp,build/%.o,${SRCS})
GRAPHS=$(patsubst src/%.cpp,%/plot.gnuplot,${SRCS})
PLOTTER=gnuplot
CFLAGS=-I src/include
CFLAGS+= -O2

all: ${PROGS}


build/%.o: src/%.cpp
	mkdir -p build
	${CXX} ${CFLAGS} -o $@ $<

%/plot.gnuplot: submit/%/graph-data.txt
	mkdir graphs -p
	cd submit; ${PLOTTER} $@

submit/%/graph-data.txt: tests/%.sh
	mkdir -p submit/$*
	$<

testgen: ${TESTS}

graphs: all testgen ${GRAPHS} 

