#!/bin/python
import random
import tempfile
from subprocess import Popen, PIPE

datafile = tempfile.TemporaryFile("w+")

cases = 1000
step = 1
datafile.write(f"{round(cases/step)}\n")

for i in range (0, cases, step):
    weight = random.randint(2000, 10000)
    datafile.write(f"{weight}")
    num_el = i*10; 
    datafile.write(f" {num_el}\n")
    for j in range (0, num_el):
        # el.append(random.randint(1, round(weight/5)) )
        # price.append(random.randint(1,weight));
        datafile.write(f"{random.randint(1, round(weight/5))} ")
        datafile.write(f"{random.randint(1,weight)}")
        datafile.write("\n")
    datafile.write("\n\n")

datafile.seek(0);
prog = Popen(['build/knapsack.o'], stdin=PIPE, stderr=PIPE, stdout=PIPE)
out, err = prog.communicate(datafile.read().encode())
prog.wait()

graph_data = open("submit/knapsack/graph-data.txt", "bw")
graph_data.write(err)
# print(out.decode('utf-8'))
